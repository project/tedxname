# TEDxName

![screenshot of TEDxName](screen_shot_v1-0_small.png "screenshot of TEDxName")


## Table of contents

* [About TEDxName](#about-tedxname)
* [About Drupal](#about-drupal)
* [Getting started](#getting-started)


## About TEDxName

TEDxName is a website for TEDx chapters. It's an open-source (**free**!) pre-designed and pre-built website that you can take and use. It can easily be installed, customised, and filled with your content.


## What it Includes

The types of content it supports:

1. **Speakers** - A person who will be speaking at a TEDx Event.
1. **Performers** - A person or group that entertains the audience, typically between talks or during intervals.
1. **Videos** - A video of a TEDx talk or performance. Should be saved on YouTube or Vimeo.
1. **Events** - A TEDx main event or related event. An event will be on one or more days, at a specific venue.
1. **Venues** - A place that can host TEDx events.
1. **Team members** - A person who helps organise a TEDx event.
1. **Sponsors** - A business or institution that provides funding or services to a TEDx event.
1. **News posts** - Time-sensitive content like news, press releases or blog posts.

Other nifty features built-in:

1. **Theme** - A professionally-designed theme specifically designed for your site.
1. **Menus** - Put any page or content into the menus, move them around, rename them... it's up to you.
1. **Users** - Choose who can log to the site, what they have permission to edit and change.
1. **Contact form** - So people can send you messages; duh.
1. **Simple interface** - Designed to be super-easy to update and keep fresh by anyone.


## How to Set it Up

For now, simply contact the developers (see below); they will be happy to set it up for you for free.

Please note that you will need to buy hosting for the website. This is typically really inexpensive, at around $5 per month.

After the developers set it up for you, you'll be able to log in and start using it.


## About the Developers

TEDxName was designed and developed by [Touchdreams](http://touchdreams.co.za), a small website design & development agency in Cape Town, South Africa.


## About the Platform

It is technically a distribution of drupal, which means that it can be downloaded and installed, and is already set-up for TEDx chapters.

Drupal is an open source content management platform supporting a variety of
websites ranging from personal blogs to large community-driven websites. For
more information, visit the [Drupal website](http://drupal.org/).
