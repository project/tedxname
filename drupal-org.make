core = 8.x
api = 2

; Modules
projects[admin_toolbar][version] = "1.18"

projects[amswap][version] = "1.1-beta1"

projects[ctools][version] = "3.0-beta1"

projects[devel][version] = "1.0-rc1"

projects[google_analytics][version] = "2.1"

projects[pathauto][version] = "1.0-rc1"

projects[pathologic][version] = "1.x-dev"

projects[token][version] = "1.0-rc1"

projects[weight][version] = "3.x-dev"
