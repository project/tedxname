/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {


// To understand behaviors, see https://drupal.org/node/756722#behaviors
Drupal.behaviors.my_custom_behavior = {
  attach: function(context, settings) {

    // Place your code here.
    $('#show-menu', context).click(function(event) {
      // $('#block-txn-main-menu').attr('aria-hidden', 'false');
      Drupal.txn.toggleAttr($('#block-txn-main-menu'), 'aria-expanded', 'true');
      Drupal.txn.toggleAttr($('body > .cover'), 'aria-hidden', 'false');
    });

    $('body > .cover', context).click(function(event) {
      close_modals();
    });

    set_up_masonry('.masonry .view-content ul', context);
    set_up_masonry_section('.field--name-field-other-images', context);

    $('.menu li button[data-is-toggle]', context).click(function(event) {
      $(this).parent().toggleClass('is-expanded');
    });
  }
};


function close_modals () {
  $('#block-txn-main-menu').attr('aria-expanded', 'false');
  $('body > .cover').attr('aria-hidden', 'true');
}


Drupal.txn = {
  toggleAttr: function(element, attr, value) {
    if (element.attr(attr) == value) {
      element.attr(attr, '');
    }
    else {
      element.attr(attr, value);
    }
  }
}

// Applies the masonry layout plugin to the artwork galleries
function set_up_masonry(container_selector) {
  var container = $(container_selector);

  container.imagesLoaded(function(){
    container.addClass('is-masonry-enabled');
    container.masonry({
      itemSelector: 'li',
      columnWidth: 'li' ,
      gutter: 0
    });
  });
}

// Applies the masonry layout plugin to the artwork galleries
function set_up_masonry_section(container_selector) {
  var container = $(container_selector);

  container.imagesLoaded(function(){
    container.addClass('is-masonry-enabled');
    container.masonry({
      itemSelector: 'div.field__item',
      columnWidth: 'div',
      gutter: 4
    });
  });
}


})(jQuery, Drupal, this, this.document);
