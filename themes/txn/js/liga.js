/* A polyfill for browsers that don't support ligatures. */
/* The script tag referring to this file must be placed before the ending body tag. */

/* To provide support for elements dynamically added, this script adds
   method 'icomoonLiga' to the window object. You can pass element references to this method.
*/
(function () {
	'use strict';
	function supportsProperty(p) {
		var prefixes = ['Webkit', 'Moz', 'O', 'ms'],
			i,
			div = document.createElement('div'),
			ret = p in div.style;
		if (!ret) {
			p = p.charAt(0).toUpperCase() + p.substr(1);
			for (i = 0; i < prefixes.length; i += 1) {
				ret = prefixes[i] + p in div.style;
				if (ret) {
					break;
				}
			}
		}
		return ret;
	}
	var icons;
	if (!supportsProperty('fontFeatureSettings')) {
		icons = {
			'home': '&#xe909;',
			'house': '&#xe909;',
			'price-tag': '&#xe90a;',
			'price-tags': '&#xe90b;',
			'phone': '&#xe90c;',
			'telephone': '&#xe90c;',
			'location': '&#xe90d;',
			'map-marker': '&#xe90d;',
			'map': '&#xe90e;',
			'guide': '&#xe90e;',
			'clock': '&#xe90f;',
			'time2': '&#xe90f;',
			'calendar': '&#xe910;',
			'date': '&#xe910;',
			'bubbles2': '&#xe911;',
			'comments2': '&#xe911;',
			'user': '&#xe912;',
			'profile2': '&#xe912;',
			'users': '&#xe913;',
			'group': '&#xe913;',
			'menu': '&#xe914;',
			'list3': '&#xe914;',
			'mail2': '&#xe915;',
			'contact2': '&#xe915;',
			'google-plus': '&#xe900;',
			'brand5': '&#xe900;',
			'facebook': '&#xe901;',
			'brand10': '&#xe901;',
			'instagram': '&#xe902;',
			'brand12': '&#xe902;',
			'twitter': '&#xe903;',
			'brand16': '&#xe903;',
			'youtube': '&#xe904;',
			'brand21': '&#xe904;',
			'vimeo': '&#xe905;',
			'brand24': '&#xe905;',
			'tumblr': '&#xe906;',
			'brand49': '&#xe906;',
			'linkedin2': '&#xe907;',
			'brand65': '&#xe907;',
			'pinterest': '&#xe908;',
			'brand72': '&#xe908;',
			'0': 0
		};
		delete icons['0'];
		window.icomoonLiga = function (els) {
			var classes,
				el,
				i,
				innerHTML,
				key;
			els = els || document.getElementsByTagName('*');
			if (!els.length) {
				els = [els];
			}
			for (i = 0; ; i += 1) {
				el = els[i];
				if (!el) {
					break;
				}
				classes = el.className;
				if (/icon-/.test(classes)) {
					innerHTML = el.innerHTML;
					if (innerHTML && innerHTML.length > 1) {
						for (key in icons) {
							if (icons.hasOwnProperty(key)) {
								innerHTML = innerHTML.replace(new RegExp(key, 'g'), icons[key]);
							}
						}
						el.innerHTML = innerHTML;
					}
				}
			}
		};
		window.icomoonLiga();
	}
}());